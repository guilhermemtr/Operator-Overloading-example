#ifndef INTEGER_H
#define INTEGER_H


class Integer
{
public:
    int n;
    Integer(int n = 0);
    virtual ~Integer();

    Integer operator+(Integer& v);
    Integer operator+(int v);
    Integer operator-(Integer& v);
    Integer operator-(int v);
    Integer operator =(Integer v);
    Integer operator =(int v);
    Integer operator ++();
    Integer operator ++(int v);
    Integer operator --();
    Integer operator --(int v);
    bool operator ==(Integer& v);
    bool operator ==(int v);
    bool operator !=(Integer& v);
    bool operator !=(int v);
    bool operator >(Integer& v);
    bool operator >(int v);
    bool operator <(Integer& v);
    bool operator <(int v);
    bool operator >=(Integer& v);
    bool operator >=(int v);
    bool operator <=(Integer& v);
    bool operator <=(int v);

    protected:
};

#endif // INTEGER_H
