#include <iostream>
#include "Integer.h"
using namespace std;

int main()
{
    Integer x(5);
    Integer y(5);

    Integer z = x++;
    Integer w = ++y;
    cout << z.n << endl;
    cout << w.n << endl;

    y++;
    z = x+y;
    w = y-x;
    cout << z.n << endl;
    cout << w.n << endl;
    ++y++;
    z = y;
    w = x;
    cout << (z > w) << endl;
    cout << (z < w) << endl;
    return 0;
}
