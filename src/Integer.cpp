#include "Integer.h"

Integer::Integer(int n)
{
    this->n = n;
}

Integer::~Integer() {}

Integer Integer::operator+(Integer& v)
{
    return Integer(this->n + v.n);
}

Integer Integer::operator+(int v)
{
    return Integer(this->n + v);
}

Integer Integer::operator-(Integer& v)
{
    return Integer(this->n - v.n);
}

Integer Integer::operator-(int v)
{
    return Integer(this->n - n);
}

Integer Integer::operator =(Integer v)
{
    this->n = v.n;
    return *this;
}

Integer Integer::operator =(int v)
{
    this->n = v;
    return *this;
}

Integer Integer::operator ++()
{
    this->n++;
    return *this;
}

Integer Integer::operator ++(int v)
{
    Integer p = *this;
    this->n++;
    return p;
}

Integer Integer::operator --()
{
    this->n--;
    return *this;
}

Integer Integer::operator --(int v)
{
    Integer p = *this;
    this->n--;
    return p;
}

bool Integer::operator ==(Integer& v)
{
    return this->n == v.n;
}

bool Integer::operator ==(int v)
{
    return this->n == v;
}

bool Integer::operator !=(Integer& v)
{
    return this->n != v.n;
}

bool Integer::operator !=(int v)
{
    return this->n != v;
}

bool Integer::operator >(Integer& v)
{
    return this->n > v.n;
}

bool Integer::operator >(int v)
{
    return this->n > v;
}

bool Integer::operator <(Integer& v)
{
    return this->n < v.n;
}

bool Integer::operator <(int v)
{
    return this->n < v;
}

bool Integer::operator >=(Integer& v)
{
    return this->n >= v.n;
}

bool Integer::operator >=(int v)
{
    return this->n >= v;
}

bool Integer::operator <=(Integer& v)
{
    return this->n <= v.n;
}

bool Integer::operator <=(int v)
{
    return this->n <= v;
}
